﻿using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Evaluating
{
    public class StandardEvaluatorTests
    {
        [Fact]
        public void Literal()
        {
            var input = new LiteralToken("hello");
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("hello", output);
        }

        [Fact]
        public void AddStringAndNumericLiterals()
        {
            var input = new BinaryOperationToken(
                new LiteralToken("4"),
                Stage2Types.PlusOperator,
                new LiteralToken(5));

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("45", output);
        }

        [Fact]
        public void MultiplyNumericLiterals()
        {
            var input = new BinaryOperationToken(
                new LiteralToken(4.0),
                Stage2Types.MultiplyOperator,
                new LiteralToken(5M));

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(20M, output);
        }

        [Fact]
        public void ModulusTest()
        {
            var input = new BinaryOperationToken(
                new LiteralToken(7),
                Stage2Types.ModulusOperator,
                new LiteralToken(5));

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(2, output);
        }

        [Fact]
        public void NegativeTest()
        {
            var input = new UnaryOperationToken(
                new LiteralToken(4),
                Stage2Types.NegativeOperator);

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(-4, output);
        }

        [Fact]
        public void NotTest()
        {
            var input = new UnaryOperationToken(
                new LiteralToken(true),
                Stage2Types.NotOperator);

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(false, output);
        }

        [Fact]
        public void ArrayDefinitionTest()
        {
            var input = new ArrayDefinitionToken(new List<EvaluableToken>()
            {
                new LiteralToken("a"),
                new LiteralToken(1),
                new LiteralToken("b")
            });

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null) as object[];
            Assert.Equal(3, output.Length);
            Assert.Equal("a", output[0]);
            Assert.Equal(1, output[1]);
            Assert.Equal("b", output[2]);
        }

        [Fact]
        public void VariableTest()
        {
            var input = new PropertyToken("test");

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => 123);
            Assert.Equal(123, output);
        }

        [Fact]
        public void PropertyTest()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new PropertyToken("Name")
            });
            var obj = new NameObj() { Name = "James" };

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => obj);
            Assert.Equal("James", output);
        }

        [Fact]
        public void PropertyIndexerTest()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new IndexerCallToken(new LiteralToken("Name"))
            });
            var obj = new NameObj() { Name = "John" };

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => obj);
            Assert.Equal("John", output);
        }

        private class NameObj
        {
            public string Name { get; set; }

            public string GetName() => Name;
        }

        [Fact]
        public void ReplaceFunctionTest()
        {
            var input = new FunctionCallToken("Replace", new List<EvaluableToken>()
            {
                new LiteralToken("Hello"),
                new LiteralToken("l"),
                new LiteralToken("Hello")
            });

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("HeHelloHelloo", output);
        }

        [Fact]
        public void ChainedReplaceFunctionTest()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new LiteralToken("Hello"),
                new FunctionCallToken("Replace", new List<EvaluableToken>()
                {
                    new LiteralToken("l"),
                    new LiteralToken("Hello")
                })
            });

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("HeHelloHelloo", output);
        }

        [Fact]
        public void CountLambdaFunctionTest()
        {
            var input = new FunctionCallToken("Count", new List<EvaluableToken>()
            {
                new PropertyToken("items"),
                new LambdaExpressionToken(new List<string>(){ "x" },
                    new BinaryOperationToken(
                        new PropertyToken("x"),
                        Stage2Types.LessThanOperator,
                        new LiteralToken(5)
                    )
                )
            });
            var obj = new List<int>() { 1, 2, 3, 4, 5, 6, 7 };

            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => obj);
            Assert.Equal(4, output);
        }

        [Fact]
        public void PropertyTest1()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new PropertyToken("Name")
            });
            var obj = new NameObj() { Name = "Billy" };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            var output = evaluator.Run(input, s => obj);
            Assert.Equal("Billy", output);
        }

        [Fact]
        public void FunctionTest1()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new FunctionCallToken("GetName", new List<EvaluableToken>())
            });
            var obj = new NameObj() { Name = "Billy" };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            evaluator.AllowObjectMethodAccess = true;
            var output = evaluator.Run(input, s => obj);
            Assert.Equal("Billy", output);
        }

        [Fact]
        public void FunctionTestWithoutPermission()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new FunctionCallToken("GetName", new List<EvaluableToken>())
            });
            var obj = new NameObj() { Name = "Billy" };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            evaluator.AllowObjectMethodAccess = false;
            Assert.Throws<Exception>(() => evaluator.Run(input, s => obj));
        }

        private class MixedObj : NameObj
        {
            public int Age { get; set; }

            public string this[string arg] => "%%" + arg + "%%";
        }

        [Fact]
        public void PropertyTest2()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new PropertyToken("Age")
            });
            var obj = new MixedObj() { Age = 35 };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            var output = evaluator.Run(input, s => obj);
            Assert.Equal(35, output);
        }

        [Fact]
        public void IndexerTest1()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new IndexerCallToken(new LiteralToken("ASDF"))
            });
            var obj = new MixedObj();

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            var output = evaluator.Run(input, s => obj);
            Assert.Equal("%%ASDF%%", output);
        }

        [Fact]
        public void IndexerTest2()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new IndexerCallToken(new LiteralToken("Age"))
            });
            var obj = new MixedObj() { Age = 50 };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            var output = evaluator.Run(input, s => obj);
            Assert.Equal("%%Age%%", output);
        }

        [Fact]
        public void IndexerTest4()
        {
            var input = new ChainToken(new List<EvaluableToken>()
            {
                new PropertyToken("myObj"),
                new IndexerCallToken(new LiteralToken(2))
            });
            var obj = new List<string>() { "abc", "def", "ghi", "jkl" };

            var evaluator = new EvalScript.Evaluating.StandardEvaluator();
            var output = evaluator.Run(input, s => obj);
            Assert.Equal("ghi", output);
        }
    }
}
