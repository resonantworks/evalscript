﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Xunit;

namespace EvalScriptTests.Evaluating
{
    public class StandardFullTests
    {
        [Fact]
        public void StringLiteralLength()
        {
            var code = "'hello'.Length";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(5, output);
        }

		[Fact]
		public void AlternativeStringLiteralTest()
		{
			var code = "`hello`";
			var input = new EvalScript.Interpreting.Interpreter() { StringLiteralChar = '`' }.Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("hello", output);
		}

		[Fact]
		public void StringPlusNull()
		{
			var code = "'hello ' + null";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("hello ", output);
		}

		[Fact]
		public void NullPlusString()
		{
			var code = "null + ' world'";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(" world", output);
		}

		[Fact]
		public void NullPlusNull()
		{
			var code = "null + null";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Null(output);
		}

		[Fact]
		public void AlternativeLambdaOperatorTest()
		{
			var code = "'hello'.Count(x :: x == 'l')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(2, output);
		}

        [Fact]
        public void NegativeFilteredArrayDefCount()
        {
            var code = "-[1, 2, 3, 4, 5].Count(x => x >= 3)";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(-3, output);
        }

        [Fact]
        public void NotAnyInArray()
        {
            var code = "!Any(items, x => x == 10)";
            var obj = new List<int>() { 1, 2, 3, 4 };
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => obj);
            Assert.Equal(true, output);
        }

		[Fact]
		public void DoesntContain()
		{
			var code = "items.Contains(10)";
			var obj = new List<int>() { 1, 2, 3, 4 };
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => obj);
			Assert.Equal(false, output);
		}

		[Fact]
		public void Contains()
		{
			var code = "[1, 3, 5, 7].Contains(3)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(true, output);
		}

		[Fact]
		public void Range()
		{
			var code = "Range(5, 5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = ((IEnumerable<int>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null)).ToList();
			Assert.Equal(5, output.Count);
			Assert.Equal(5, output[0]);
			Assert.Equal(6, output[1]);
			Assert.Equal(7, output[2]);
			Assert.Equal(8, output[3]);
			Assert.Equal(9, output[4]);
		}

		[Fact]
		public void SelectMany()
		{
			var code = "[5, 7, 9].SelectMany(x => [1*x, 2*x, 3*x])";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(9, output.Count);
			Assert.Equal(5, output[0]);
			Assert.Equal(10, output[1]);
			Assert.Equal(15, output[2]);
			Assert.Equal(7, output[3]);
			Assert.Equal(14, output[4]);
			Assert.Equal(21, output[5]);
			Assert.Equal(9, output[6]);
			Assert.Equal(18, output[7]);
			Assert.Equal(27, output[8]);
		}

		[Fact]
		public void SingleArrayMin()
		{
			var code = "[6].Min(a => a)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(6, output);
		}

		[Fact]
		public void SelectManyWithNulls()
		{
			var code = "[1, 2, 3].SelectMany(x => [x, null])";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(6, output.Count);
			Assert.Equal(1, output[0]);
			Assert.Null(output[1]);
			Assert.Equal(2, output[2]);
			Assert.Null(output[3]);
			Assert.Equal(3, output[4]);
			Assert.Null(output[5]);
		}

		[Fact]
		public void OrderBySingleArg()
		{
			var code = "['greetings', 'hello', 'hey', 'hi', 'howdy'].OrderBy(x => x.Length)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(5, output.Count);
			Assert.Equal("hi", output[0]);
			Assert.Equal("hey", output[1]);
			Assert.Equal("hello", output[2]);
			Assert.Equal("howdy", output[3]);
			Assert.Equal("greetings", output[4]);
		}

		[Fact]
		public void OrderByTwoArgs()
		{
			var code = "['salutations', 'sup', 'morning', 'hey', 'pip pip', 'cheerio', 'hello'].OrderBy(x => x.Length, x => x)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(7, output.Count);
			Assert.Equal("hey", output[0]);
			Assert.Equal("sup", output[1]);
			Assert.Equal("hello", output[2]);
			Assert.Equal("cheerio", output[3]);
			Assert.Equal("morning", output[4]);
			Assert.Equal("pip pip", output[5]);
			Assert.Equal("salutations", output[6]);
		}

		[Fact]
		public void OrderInDifferentDirections()
		{
			var code = "['salutations', 'sup', 'morning', 'hey', 'pip pip', 'cheerio', 'hello'].OrderBy(x => x.Length, (x, desc) => x)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(7, output.Count);
			Assert.Equal("sup", output[0]);
			Assert.Equal("hey", output[1]);
			Assert.Equal("hello", output[2]);
			Assert.Equal("pip pip", output[3]);
			Assert.Equal("morning", output[4]);
			Assert.Equal("cheerio", output[5]);
			Assert.Equal("salutations", output[6]);
		}

		[Fact]
		public void OrderDescBySingleArg()
		{
			var code = "['greetings', 'hello', 'hey', 'hi', 'howdy'].OrderByDesc(x => x.Length)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(5, output.Count);
			Assert.Equal("greetings", output[0]);
			Assert.Equal("hello", output[1]);
			Assert.Equal("howdy", output[2]);
			Assert.Equal("hey", output[3]);
			Assert.Equal("hi", output[4]);
		}

		[Fact]
		public void OrderDescByTwoArgs()
		{
			var code = "['salutations', 'sup', 'morning', 'hey', 'pip pip', 'cheerio', 'hello'].OrderByDesc(x => x.Length, x => x)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(7, output.Count);
			Assert.Equal("salutations", output[0]);
			Assert.Equal("pip pip", output[1]);
			Assert.Equal("morning", output[2]);
			Assert.Equal("cheerio", output[3]);
			Assert.Equal("hello", output[4]);
			Assert.Equal("sup", output[5]);
			Assert.Equal("hey", output[6]);
		}

		[Fact]
		public void OrderDescInDifferentDirections()
		{
			var code = "['salutations', 'sup', 'morning', 'hey', 'pip pip', 'cheerio', 'hello'].OrderByDesc(x => x.Length, (x, asc) => x)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(7, output.Count);
			Assert.Equal("salutations", output[0]);
			Assert.Equal("cheerio", output[1]);
			Assert.Equal("morning", output[2]);
			Assert.Equal("pip pip", output[3]);
			Assert.Equal("hello", output[4]);
			Assert.Equal("hey", output[5]);
			Assert.Equal("sup", output[6]);
		}

		[Fact]
		public void DistinctWithInts()
		{
			var code = "[1, 5, 3, 7, 2, 5, 3].Distinct()";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(5, output.Count);
			Assert.Equal(1, output[0]);
			Assert.Equal(5, output[1]);
			Assert.Equal(3, output[2]);
			Assert.Equal(7, output[3]);
			Assert.Equal(2, output[4]);
		}

		[Fact]
		public void DistinctWithMixedData()
		{
			var code = "[5, null, 'hi', 'boo', 5, 3, null, 'hi'].Distinct()";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (List<object>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(5, output.Count);
			Assert.Equal(5, output[0]);
			Assert.Null(output[1]);
			Assert.Equal("hi", output[2]);
			Assert.Equal("boo", output[3]);
			Assert.Equal(3, output[4]);
		}

        [Fact]
        public void TrimString()
        {
            var code = "'  hi  '.Trim()";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("hi", output);
        }

        [Fact]
        public void TrimToUpperString()
        {
            var code = "'  hi  '.Trim().ToUpper()";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("HI", output);
        }

        [Fact]
        public void StringIndexOf1()
        {
            var code = "'abcde'.IndexOf('cd')";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(2, output);
        }

        [Fact]
        public void StringIndexOf2()
        {
            var code = "'12345'.IndexOf(4)";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(3, output);
        }

		[Fact]
        public void StringIndexOf3()
        {
            var code = "'Hello James'.IndexOf('e', 5)";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(9, output);
		}

		[Fact]
		public void StringLastIndexOf1()
		{
			var code = "'abcde'.LastIndexOf('cd')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(2, output);
		}

		[Fact]
		public void StringLastIndexOf2()
		{
			var code = "'14345'.LastIndexOf(4)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(3, output);
		}

		[Fact]
		public void StringLastIndexOf3()
		{
			var code = "'Hello James'.LastIndexOf('e', 5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(1, output);
		}

		[Fact]
        public void StringSplit1()
        {
            var code = "Split('I am not a robot', ' ')";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = (List<string>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(5, output.Count);
            Assert.Equal("I", output[0]);
            Assert.Equal("am", output[1]);
            Assert.Equal("not", output[2]);
            Assert.Equal("a", output[3]);
            Assert.Equal("robot", output[4]);
        }

        [Fact]
        public void StringSplit2()
        {
            var code = "myVar.Split('a', 'c')";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = (List<string>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => "abracadabra");
            Assert.Equal(7, output.Count);
            Assert.Equal("", output[0]);
            Assert.Equal("br", output[1]);
            Assert.Equal("", output[2]);
            Assert.Equal("", output[3]);
            Assert.Equal("d", output[4]);
            Assert.Equal("br", output[5]);
            Assert.Equal("", output[6]);
        }

        [Fact]
        public void StringSplit3()
        {
            var code = "'0123456789'.Split(3, 5)";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = (List<string>)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(3, output.Count);
            Assert.Equal("012", output[0]);
            Assert.Equal("4", output[1]);
            Assert.Equal("6789", output[2]);
        }

		[Fact]
		public void StringInsert1()
		{
			var code = "'howdy'.Insert(2, 'wdy ho')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (string)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("howdy howdy", output);
		}

		[Fact]
		public void StringInsert2()
		{
			var code = "Insert('RD2', 1, 2)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (string)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("R2D2", output);
		}

		[Fact]
		public void StringInsert3()
		{
			var code = "'abc'.Insert(3, 'd')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (string)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("abcd", output);
		}

		[Fact]
		public void StringInsert4()
		{
			var code = "'abc'.Insert(4, 'd')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void StringInsert5()
		{
			var code = "'abc'.Insert(-1, 'd')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void StringRemove1()
		{
			var code = "'0123456789'.Remove(5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (string)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("01234", output);
		}

		[Fact]
		public void StringRemove2()
		{
			var code = "'0123456789'.Remove(5, 2)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (string)new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("01234789", output);
		}

		[Fact]
        public void DictionaryProperty()
        {
            var code = "[FirstName:'James', LastName:'Coyle'].LastName";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("Coyle", output);
        }

        [Fact]
        public void FormatTest()
        {
            var code = "1.0 / 3 #0.0";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("0.3", output);
        }

        [Fact]
        public void ArrayIndexer()
        {
            var code = "obj[0]";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => new object[] { "hello", 1, null });
            Assert.Equal("hello", output);
        }

        [Fact]
        public void TernaryOperator()
        {
            var code = "obj ? 3 : 5";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => true);
            Assert.Equal(3, output);
        }

        [Fact]
        public void AsOperator()
        {
            var code = "'2019/8/29' as DateTime";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal(new DateTime(2019, 8, 29), output);
        }

		[Fact]
		public void IndexerAccessOfDictionaryItem()
		{
			var code = "dict['height']";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => new Dictionary<string, object>() { { "height", 1.5 } });
			Assert.Equal(1.5, output);
		}

		[Fact]
		public void TrySuccess()
		{
			var code = "Try(() => 'hello'.Length)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(5, output);
		}

		[Fact]
		public void TryFailure1()
		{
			var code = "Try(() => 'hello'.Longth, () => 'Uh oh')";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("Uh oh", output);
		}

		[Fact]
		public void TryFailure2()
		{
			var code = "Try(() => 'hello'.Longth)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Null(output);
		}

		private class TestObj
		{
			public string Name { get; set; }

			public Dictionary<string, object> AdditionalValues { get; private set; } = new Dictionary<string, object>();
		}

		[Fact]
		public void PropertyIndexerTest()
		{
			var code = "myObj.AdditionalValues['Colour']";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var myObj = new TestObj();
			myObj.AdditionalValues["Colour"] = "Red";
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => myObj);
			Assert.Equal("Red", output);
		}

		private class TestObj2
		{
			public TestObj TestObj { get; set; } = new TestObj();
		}

		[Fact]
		public void PropertyPropertyIndexerTest()
		{
			var code = "myObj.TestObj.AdditionalValues['Colour']";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var myObj = new TestObj2();
			myObj.TestObj.AdditionalValues["Colour"] = "Red";
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => myObj);
			Assert.Equal("Red", output);
		}

		[Fact]
		public void MergeToStringTest()
		{
			var code = "MergeToString([[FirstName:'John', LastName:'Smith'], [FirstName:'Michael', LastName:'Jones']], ', ', x => x.FirstName + ' ' + x.LastName)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("John Smith, Michael Jones", output);
		}

		[Fact]
		public void CharTest()
		{
			var code = "Char(75)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("K", output);
		}

		[Fact]
		public void CompareDecimalLessThanDouble()
		{
			var code = "x < 123.1";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => 120.9M);
			Assert.Equal(true, output);
		}

		[Fact]
		public void CompareDecimalEqualDouble()
		{
			var code = "x == 123.1";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => 123.1M);
			Assert.Equal(true, output);
		}

		[Fact]
		public void CompareLargeDecimalGreaterThanDouble()
		{
			var code = "x > 79228162514264337593543950350.0";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => 79228162514264337593543950335M);
			Assert.Equal(false, output);
		}

		[Fact]
		public void PowerWithDecimal()
		{
			var code = "x ^ 2";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => 5M);
			Assert.Equal(25.0, output);
		}

		[Fact]
		public void RandomIntDifferentOutputs()
		{
			var code = "RandomInt()";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output1 = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			var output2 = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			var output3 = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output1 != output2 || output2 != output3);
		}

		[Fact]
		public void RandomIntLessThanMax()
		{
			var code = "RandomInt(5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output < 5);
		}

		[Fact]
		public void RandomIntNegativeMax()
		{
			var code = "RandomInt(-5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void RandomIntMaxLessThanMin()
		{
			var code = "RandomInt(5, 3)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void RandomIntBetweenMinAndMax()
		{
			var code = "RandomInt(3, 5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output >= 3 && output < 5);
		}

		[Fact]
		public void RandomIntBetweenNegativeMinAndMax()
		{
			var code = "RandomInt(-5, -3)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (int)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output >= -5 && output < -3);
		}

		[Fact]
		public void RandomDoubleLessThanMax()
		{
			var code = "RandomDouble(5.3)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (double)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output < 5.3);
		}

		[Fact]
		public void RandomDoubleNegativeMax()
		{
			var code = "RandomDouble(-5.6)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void RandomDoubleMaxLessThanMin()
		{
			var code = "RandomDouble(5.1, 3.2)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			Assert.Throws<ArgumentOutOfRangeException>(() => new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
		}

		[Fact]
		public void RandomDoubleBetweenMinAndMax()
		{
			var code = "RandomDouble(3.2, 5.1)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (double)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output >= 3.2 && output < 5.1);
		}

		[Fact]
		public void RandomDoubleBetweenNegativeMinAndMax()
		{
			var code = "RandomDouble(-5.8, -3.7)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = (double)(new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null));
			Assert.True(output >= -5.8 && output < -3.7);
		}

		[Fact]
		public void TernaryOnFunctionCall()
		{
			var code = "IsNullOrEmpty('hello') ? 'hi' : 'bye'";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal("bye", output);
		}

		[Fact]
		public void PartitionPerfectFit()
		{
			var code = "[1,2,3,4,5,6].Partition(3)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Equal(2, list.Count);
			Assert.Equal(3, list[0].Length);
			Assert.Equal(3, list[1].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[0][2]);
			Assert.Equal(4, list[1][0]);
			Assert.Equal(5, list[1][1]);
			Assert.Equal(6, list[1][2]);
		}

		[Fact]
		public void PartitionPerfectFitOnlyFull()
		{
			var code = "[1,2,3,4,5,6,7,8].Partition(4, true)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Equal(2, list.Count);
			Assert.Equal(4, list[0].Length);
			Assert.Equal(4, list[1].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[0][2]);
			Assert.Equal(4, list[0][3]);
			Assert.Equal(5, list[1][0]);
			Assert.Equal(6, list[1][1]);
			Assert.Equal(7, list[1][2]);
			Assert.Equal(8, list[1][3]);
		}

		[Fact]
		public void PartitionImperfectFit1()
		{
			var code = "[1,2,3,4,5].Partition(2)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Equal(3, list.Count);
			Assert.Equal(2, list[0].Length);
			Assert.Equal(2, list[1].Length);
			Assert.Equal(2, list[2].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[1][0]);
			Assert.Equal(4, list[1][1]);
			Assert.Equal(5, list[2][0]);
			Assert.Null(list[2][1]);
		}

		[Fact]
		public void PartitionImperfectFit2()
		{
			var code = "[1,2,3,4,5,6,7].Partition(5)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Equal(2, list.Count);
			Assert.Equal(5, list[0].Length);
			Assert.Equal(5, list[1].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[0][2]);
			Assert.Equal(4, list[0][3]);
			Assert.Equal(5, list[0][4]);
			Assert.Equal(6, list[1][0]);
			Assert.Equal(7, list[1][1]);
			Assert.Null(list[1][2]);
			Assert.Null(list[1][3]);
			Assert.Null(list[1][4]);
		}

		[Fact]
		public void PartitionImperfectFitOnlyFull1()
		{
			var code = "[1,2,3,4,5].Partition(2, true)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Equal(2, list.Count);
			Assert.Equal(2, list[0].Length);
			Assert.Equal(2, list[1].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[1][0]);
			Assert.Equal(4, list[1][1]);
		}

		[Fact]
		public void PartitionImperfectFitOnlyFull2()
		{
			var code = "[1,2,3,4,5,6,7].Partition(5, true)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			var list = output as List<object[]>;
			Assert.Single(list);
			Assert.Equal(5, list[0].Length);
			Assert.Equal(1, list[0][0]);
			Assert.Equal(2, list[0][1]);
			Assert.Equal(3, list[0][2]);
			Assert.Equal(4, list[0][3]);
			Assert.Equal(5, list[0][4]);
		}

		[Fact]
		public void SumWithSelector()
		{
			var code = "['hello', 'James'].Sum(x => x.Length)";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(10, output);
		}

		[Fact]
		public void SumWithoutSelector()
		{
			var code = "[1,2,3,4,5,6].Sum()";
			var input = new EvalScript.Interpreting.Interpreter().Run(code);
			var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
			Assert.Equal(21, output);
		}
	}
}
