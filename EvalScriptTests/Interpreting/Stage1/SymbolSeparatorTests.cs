﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage1
{
    public class SymbolSeparatorTests
    {
        [Fact]
        public void GreaterOrEqualTest()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "5 >= 4")
            };
            var output = new SymbolSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(4, output.Count);
            Assert.Equal("5 ", output[0].Value);
            Assert.Equal(Stage1Types.RightAngleBracket, output[1].Type);
            Assert.Equal(Stage1Types.Equal, output[2].Type);
            Assert.Equal(" 4", output[3].Value);
        }
    }
}
