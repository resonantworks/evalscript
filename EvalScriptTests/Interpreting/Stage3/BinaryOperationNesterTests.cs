﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage3;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage3
{
    public class BinaryOperationNesterTests
    {
        [Fact]
        public void Simple()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.NumericLiteral, 2),
                new Token(Stage2Types.PlusOperator),
                new Token(Stage2Types.NumericLiteral, 3)
            };
            new BinaryOperationNester(Stage2Types.PlusOperator).Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage3Types.Operation, input[0].Type);
            var op = input[0] as ParentToken;
            Assert.Equal(3, op.Children.Count);
        }

        [Fact]
        public void PlusAndMultiply()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.NumericLiteral, 5),
                new Token(Stage2Types.MultiplyOperator),
                new Token(Stage2Types.NumericLiteral, 2),
                new Token(Stage2Types.PlusOperator),
                new Token(Stage2Types.NumericLiteral, 3)
            };
            new BinaryOperationNester(Stage2Types.MultiplyOperator).Run(new EvalScript.Interpreting.Interpreter(), input);
            new BinaryOperationNester(Stage2Types.PlusOperator).Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage3Types.Operation, input[0].Type);
            var op1 = input[0] as ParentToken;
            Assert.Equal(3, op1.Children.Count);
            Assert.Equal(Stage2Types.PlusOperator, op1.Children[1].Type);
            var op2 = op1.Children[0] as ParentToken;
            Assert.Equal(3, op2.Children.Count);
            Assert.Equal(Stage2Types.MultiplyOperator, op2.Children[1].Type);
        }

        [Fact]
        public void ModulusAndEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.NumericLiteral, 3),
                new Token(Stage2Types.ModulusOperator),
                new Token(Stage2Types.NumericLiteral, 2),
                new Token(Stage2Types.EqualOperator),
                new Token(Stage2Types.NumericLiteral, 1)
            };
            new BinaryOperationNester(Stage2Types.ModulusOperator).Run(new EvalScript.Interpreting.Interpreter(), input);
            new BinaryOperationNester(Stage2Types.EqualOperator).Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage3Types.Operation, input[0].Type);
            var op1 = input[0] as ParentToken;
            Assert.Equal(3, op1.Children.Count);
            Assert.Equal(Stage2Types.EqualOperator, op1.Children[1].Type);
            var op2 = op1.Children[0] as ParentToken;
            Assert.Equal(3, op2.Children.Count);
            Assert.Equal(Stage2Types.ModulusOperator, op2.Children[1].Type);
        }
    }
}
