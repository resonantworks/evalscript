﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage4;
using EvalScript.Interpreting.Stage2;
using System.Linq;

namespace EvalScriptTests.Interpreter
{
    public class InterpreterTests
    {
        [Fact]
        public void Add()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Model.Height + 5");
            var binaryOp = output as BinaryOperationToken;
            Assert.Equal(Stage2Types.PlusOperator, binaryOp.Type);
            Assert.Equal(5, binaryOp.Right.Value);
            var chain = binaryOp.Left as ChainToken;
            Assert.Equal(2, chain.Items.Count());
            Assert.Equal("Model", (chain.Items.ToList()[0] as PropertyToken).Name);
            Assert.Equal("Height", (chain.Items.ToList()[1] as PropertyToken).Name);
        }

        [Fact]
        public void MultiplyFunctionCall()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Model.Lines.Count(x => x.Width > 10) * 4");
            var binaryOp = output as BinaryOperationToken;
            Assert.Equal(Stage2Types.MultiplyOperator, binaryOp.Type);
            Assert.Equal(4, binaryOp.Right.Value);
            var chain = binaryOp.Left as ChainToken;
            Assert.Equal(3, chain.Items.Count());
            Assert.Equal("Model", (chain.Items.ToList()[0] as PropertyToken).Name);
            Assert.Equal("Lines", (chain.Items.ToList()[1] as PropertyToken).Name);
            var function = chain.Items.ToList()[2] as FunctionCallToken;
            Assert.Equal("Count", function.Name);
            var lambda = function.Parameters.First() as LambdaExpressionToken;
            Assert.Single(lambda.Parameters);
            Assert.Equal("x", lambda.Parameters.First());
            var lambdaBinaryOp = lambda.Body as BinaryOperationToken;
            Assert.Equal(Stage2Types.GreaterThanOperator, lambdaBinaryOp.Type);
            Assert.Equal(10, lambdaBinaryOp.Right.Value);
            var lambdaChain = lambdaBinaryOp.Left as ChainToken;
            Assert.Equal(2, lambdaChain.Items.Count());
            Assert.Equal("x", (lambdaChain.Items.ToList()[0] as PropertyToken).Name);
            Assert.Equal("Width", (lambdaChain.Items.ToList()[1] as PropertyToken).Name);
        }

        [Fact]
        public void NestedOperations()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("2 + (3 * 4)");
            var binaryOp = output as BinaryOperationToken;
            Assert.Equal(Stage2Types.PlusOperator, binaryOp.Type);
            var nestedOp = binaryOp.Right as BinaryOperationToken;
            Assert.Equal(Stage2Types.MultiplyOperator, nestedOp.Type);
        }

		[Fact]
		public void UnnestedOperations()
		{
			var output = new EvalScript.Interpreting.Interpreter().Run("2 + 3 + 4");
			var binaryOp = output as BinaryOperationToken;
			Assert.Equal(Stage2Types.PlusOperator, binaryOp.Type);
			var nestedOp = binaryOp.Left as BinaryOperationToken;
			Assert.Equal(Stage2Types.PlusOperator, nestedOp.Type);
		}

		[Fact]
		public void UnnestedStringConcat()
		{
			var output = new EvalScript.Interpreting.Interpreter().Run("obj1 + ' ' + obj2");
			var binaryOp = output as BinaryOperationToken;
			Assert.Equal(Stage2Types.PlusOperator, binaryOp.Type);
			var nestedOp = binaryOp.Left as BinaryOperationToken;
			Assert.Equal(Stage2Types.PlusOperator, nestedOp.Type);
		}

        [Fact]
        public void MultiplyByNegative()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("5 * -2");
            var binaryOp = output as BinaryOperationToken;
            Assert.Equal(Stage2Types.MultiplyOperator, binaryOp.Type);
            var unaryOp = binaryOp.Right as UnaryOperationToken;
            Assert.Equal(Stage2Types.NegativeOperator, unaryOp.Type);
            Assert.Equal(2, unaryOp.Operand.Value);
        }

        [Fact]
        public void ArrayDefinition()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("[1, 2, 3]");
            var array = output as ArrayDefinitionToken;
            Assert.Equal(3, array.Items.Count());
        }

        [Fact]
        public void DictionaryDefinition()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("[a:1, b:2, c:3]");
            var dict = output as DictionaryDefinitionToken;
            Assert.Equal(3, dict.Items.Count());
        }

        [Fact]
        public void BinaryOpInDictionaryDef()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("[a:3 + 2, b:4 < 5]");
            var dict = output as DictionaryDefinitionToken;
            Assert.Equal(2, dict.Items.Count());

            var keyVal0 = dict.Items.ToList()[0];
            Assert.IsType<BinaryOperationToken>(keyVal0.ValueToken);

            var keyVal1 = dict.Items.ToList()[1];
            Assert.IsType<BinaryOperationToken>(keyVal1.ValueToken);
        }

        [Fact]
        public void FormattedResult()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("1.0 / 3 #0.0");
            var format = output as FormattingToken;
            Assert.Equal("0.0", format.Format);

            var calc = format.Input as BinaryOperationToken;
            Assert.Equal(Stage2Types.DivideOperator, calc.Type);

            var left = calc.Left as LiteralToken;
            var right = calc.Right as LiteralToken;
            Assert.Equal(1.0, left.Value);
            Assert.Equal(3, right.Value);
        }

        [Fact]
        public void CompareCalculations()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("3 * 4 > 10 - 1");
            var comparison = output as BinaryOperationToken;
            Assert.Equal(Stage2Types.GreaterThanOperator, comparison.Type);

            var mult = comparison.Left as BinaryOperationToken;
            Assert.Equal(Stage2Types.MultiplyOperator, mult.Type);
            Assert.Equal(3, mult.Left.Value);
            Assert.Equal(4, mult.Right.Value);

            var minus = comparison.Right as BinaryOperationToken;
            Assert.Equal(Stage2Types.MinusOperator, minus.Type);
            Assert.Equal(10, minus.Left.Value);
            Assert.Equal(1, minus.Right.Value);
        }

		[Fact]
		public void LambdaBinaryOp()
		{
			var output = new EvalScript.Interpreting.Interpreter().Run("x => x.Value + 3");
			var lambda = output as LambdaExpressionToken;
			var binaryOp = lambda.Body as BinaryOperationToken;
			Assert.Equal(Stage2Types.PlusOperator, binaryOp.Type);
			Assert.Single(lambda.Parameters);
			Assert.Equal("x", lambda.Parameters.First());
		}

		[Fact]
		public void LambdaMultipleUnnestedBinaryOps()
		{
			var output = new EvalScript.Interpreting.Interpreter().Run("x => x.FirstName + ' ' + x.LastName");
			var lambda = output as LambdaExpressionToken;
			var op1 = lambda.Body as BinaryOperationToken;
			var op2 = op1.Left ?? op1.Right;
			Assert.Equal(Stage2Types.PlusOperator, op1.Type);
			Assert.Equal(Stage2Types.PlusOperator, op2.Type);
			Assert.Single(lambda.Parameters);
			Assert.Equal("x", lambda.Parameters.First());
		}

        [Fact]
        public void BinaryOpInParam1()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Max(1 + 2, 1, 4)");
            var func = output as FunctionCallToken;
            var parm1 = func.Parameters.First() as BinaryOperationToken;
            var parm2 = func.Parameters.ToList()[1] as LiteralToken;
            var parm3 = func.Parameters.Last() as LiteralToken;
            var binOpLeft = parm1.Left as LiteralToken;
            var binOpRight = parm1.Right as LiteralToken;

            Assert.Equal("Max", func.Name);
            Assert.Equal(3, func.Parameters.Count());
            Assert.Equal(Stage2Types.PlusOperator, parm1.Type);
            Assert.Equal(1, binOpLeft.Value);
            Assert.Equal(2, binOpRight.Value);
            Assert.Equal(1, parm2.Value);
            Assert.Equal(4, parm3.Value);
        }

        [Fact]
        public void BinaryOpInParam2()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Max(1, 1 + 2, 4)");
            var func = output as FunctionCallToken;
            var parm1 = func.Parameters.First() as LiteralToken;
            var parm2 = func.Parameters.ToList()[1] as BinaryOperationToken;
            var parm3 = func.Parameters.Last() as LiteralToken;
            var binOpLeft = parm2.Left as LiteralToken;
            var binOpRight = parm2.Right as LiteralToken;

            Assert.Equal("Max", func.Name);
            Assert.Equal(3, func.Parameters.Count());
            Assert.Equal(1, parm1.Value);
            Assert.Equal(Stage2Types.PlusOperator, parm2.Type);
            Assert.Equal(1, binOpLeft.Value);
            Assert.Equal(2, binOpRight.Value);
            Assert.Equal(4, parm3.Value);
        }

        [Fact]
        public void BinaryOpInParam3()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Max(1, 4, 1 + 2)");
            var func = output as FunctionCallToken;
            var parm1 = func.Parameters.First() as LiteralToken;
            var parm2 = func.Parameters.ToList()[1] as LiteralToken;
            var parm3 = func.Parameters.Last() as BinaryOperationToken;
            var binOpLeft = parm3.Left as LiteralToken;
            var binOpRight = parm3.Right as LiteralToken;

            Assert.Equal("Max", func.Name);
            Assert.Equal(3, func.Parameters.Count());
            Assert.Equal(1, parm1.Value);
            Assert.Equal(4, parm2.Value);
            Assert.Equal(Stage2Types.PlusOperator, parm3.Type);
            Assert.Equal(1, binOpLeft.Value);
            Assert.Equal(2, binOpRight.Value);
        }
    }
}
