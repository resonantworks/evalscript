﻿using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class LambdaExpressionBuilderTests
    {
        [Fact]
        public void Simple()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("x => x.Width < x.Height");
            var lambda = output as LambdaExpressionToken;
            Assert.Single(lambda.Parameters);
            var binaryOp = lambda.Body as BinaryOperationToken;
            Assert.Equal(Stage2Types.LessThanOperator, binaryOp.Type);
            var left = binaryOp.Left as ChainToken;
            Assert.Equal(2, left.Items.Count());
            var right = binaryOp.Right as ChainToken;
            Assert.Equal(2, right.Items.Count());
        }

		[Fact]
		public void TwoParams()
		{
			var output = new EvalScript.Interpreting.Interpreter().Run("(x, y) => x < y");
			var lambda = output as LambdaExpressionToken;
			Assert.Equal(2, lambda.Parameters.Count());
			Assert.Equal("x", lambda.Parameters.ToList()[0]);
			Assert.Equal("y", lambda.Parameters.ToList()[1]);
			var binaryOp = lambda.Body as BinaryOperationToken;
			Assert.Equal(Stage2Types.LessThanOperator, binaryOp.Type);
			var left = binaryOp.Left as PropertyToken;
			var right = binaryOp.Right as PropertyToken;
		}
    }
}
