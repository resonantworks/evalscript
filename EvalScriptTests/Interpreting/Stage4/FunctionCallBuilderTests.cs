﻿using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class FunctionCallBuilderTests
    {
        [Fact]
        public void Simple()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("Count()");
            var function = output as FunctionCallToken;
            Assert.Equal("Count", function.Name);
            Assert.Empty(function.Parameters);
        }
    }
}
