﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class BinaryOperationBuilderTests
    {
        [Fact]
        public void SimpleBinaryOperation()
        {
            var input = new List<Token>()
            {
                new LiteralToken(5),
                new Token(Stage2Types.MinusOperator),
                new LiteralToken(3)
            };
            new BinaryOperationBuilder().Run(new EvalScript.Interpreting.Interpreter(), input);
            var output = input[0] as BinaryOperationToken;
            Assert.Equal(5, output.Left.Value);
            Assert.Equal(3, output.Right.Value);
            Assert.Equal(Stage2Types.MinusOperator, output.Type);
        }
    }
}
