﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage3;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class ArrayDefinitionBuilderTests
    {
        [Fact]
        public void SimpleDef()
        {
            var input = new List<Token>()
            {
                new ParentToken(Stage3Types.SquareBrackets,
                    new LiteralToken(1),
                    new Token(Stage2Types.Comma),
                    new LiteralToken(2),
                    new Token(Stage2Types.Comma),
                    new LiteralToken(3)
                )
            };
            new ArrayDefinitionBuilder().Run(new EvalScript.Interpreting.Interpreter(), input);
            var output = input[0] as ArrayDefinitionToken;
            Assert.Equal(3, output.Items.Count());
            Assert.Equal(1, output.Items.ToList()[0].Value);
            Assert.Equal(2, output.Items.ToList()[1].Value);
            Assert.Equal(3, output.Items.ToList()[2].Value);
        }
    }
}
