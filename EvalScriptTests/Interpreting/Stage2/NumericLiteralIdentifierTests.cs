﻿using EvalScript;
using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class NumericLiteralIdentifierTests
    {
        [Fact]
        public void IntLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123, input[0].Value);
        }

        [Fact]
        public void LongLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123L")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123L, input[0].Value);
        }

        [Fact]
        public void DoubleLiteral1()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123"),
                new Token(Stage1Types.Dot),
                new Token(Stage1Types.Text, "45")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123.45, input[0].Value);
        }

        [Fact]
        public void DoubleLiteral2()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123"),
                new Token(Stage1Types.Dot),
                new Token(Stage1Types.Text, "45D")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123.45, input[0].Value);
        }

        [Fact]
        public void FloatLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123"),
                new Token(Stage1Types.Dot),
                new Token(Stage1Types.Text, "45F")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123.45F, input[0].Value);
        }

        [Fact]
        public void DecimalLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "123"),
                new Token(Stage1Types.Dot),
                new Token(Stage1Types.Text, "45M")
            };
            new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(123.45M, input[0].Value);
        }

        [Fact]
        public void InvalidLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "12F4")
            };
            Assert.Throws<SyntaxException>(() => new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input));
        }

		[Fact]
		public void DecimalLiteralInFrench()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");
			var input = new List<Token>()
			{
				new Token(Stage1Types.Text, "123"),
				new Token(Stage1Types.Dot),
				new Token(Stage1Types.Text, "45M")
			};
			new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
			Assert.Single(input);
			Assert.Equal(123.45M, input[0].Value);
		}

		[Fact]
		public void IntLiteralInHebrew()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("he-IL");
			var input = new List<Token>()
			{
				new Token(Stage1Types.Text, "123")
			};
			new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
			Assert.Single(input);
			Assert.Equal(123, input[0].Value);
		}

		[Fact]
		public void LongLiteralInFinnish()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fi-FI");
			var input = new List<Token>()
			{
				new Token(Stage1Types.Text, "123L")
			};
			new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
			Assert.Single(input);
			Assert.Equal(123L, input[0].Value);
		}

		[Fact]
		public void DoubleLiteralInCroatian()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("hr-HR");
			var input = new List<Token>()
			{
				new Token(Stage1Types.Text, "123"),
				new Token(Stage1Types.Dot),
				new Token(Stage1Types.Text, "45")
			};
			new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
			Assert.Single(input);
			Assert.Equal(123.45, input[0].Value);
		}

		[Fact]
		public void FloatLiteralInIcelandic()
		{
			Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("is-IS");
			var input = new List<Token>()
			{
				new Token(Stage1Types.Text, "123"),
				new Token(Stage1Types.Dot),
				new Token(Stage1Types.Text, "45F")
			};
			new NumericLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
			Assert.Single(input);
			Assert.Equal(123.45F, input[0].Value);
		}
	}
}
