﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class NegativeOperatorIdentifierTests
    {
        [Fact]
        public void AfterOperator()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.PlusOperator),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[1].Type);
        }

        [Fact]
        public void AfterLambdaOperator()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.LambdaOperator),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[1].Type);
        }

        [Fact]
        public void StandardMinus()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.NumericLiteral, 15),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.MinusOperator, input[1].Type);
        }

        [Fact]
        public void AtStart()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[0].Type);
        }

        [Fact]
        public void AfterLeftBracket()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.LeftBracket),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[1].Type);
        }

        [Fact]
        public void AfterRightBracket()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.RightBracket),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.MinusOperator, input[1].Type);
        }

        [Fact]
        public void AfterComma()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.Comma),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[1].Type);
        }

        [Fact]
        public void AfterColon()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.KeyValueOperator),
                new Token(Stage2Types.MinusOperator),
                new Token(Stage2Types.NumericLiteral, 10)
            };
            new NegativeOperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage2Types.NegativeOperator, input[1].Type);
        }
    }
}
