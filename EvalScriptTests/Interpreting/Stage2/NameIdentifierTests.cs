﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class NameIdentifierTests
    {
        [Fact]
        public void LoneName()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
        }

        [Fact]
        public void NameDotName()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name"),
                new Token(Stage1Types.Dot),
                new Token(Stage1Types.Text, "name")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal("name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
            Assert.Equal("name", input[2].Value);
            Assert.Equal(Stage2Types.PropertyName, input[2].Type);
        }

        [Fact]
        public void NameStartingWithTilde()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "~name")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("~name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
        }

        [Fact]
        public void NameStartingWithAt()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "@name")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("@name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
        }

        [Fact]
        public void IgnoreNameContainingTilde()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "na~me")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("na~me", input[0].Value);
            Assert.Equal(Stage1Types.Text, input[0].Type);
        }

        [Fact]
        public void IgnoreNameContainingAt()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "na@me")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("na@me", input[0].Value);
            Assert.Equal(Stage1Types.Text, input[0].Type);
        }

        [Fact]
        public void NameRightBracket()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name"),
                new Token(Stage1Types.RightBracket)
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal("name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
        }

        [Fact]
        public void NameLeftBracket()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name"),
                new Token(Stage1Types.LeftBracket)
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal("name", input[0].Value);
            Assert.Equal(Stage2Types.FunctionName, input[0].Type);
        }

        [Fact]
        public void NameLeftSquareBracket()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name"),
                new Token(Stage1Types.LeftSquareBracket)
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal("name", input[0].Value);
            Assert.Equal(Stage2Types.PropertyName, input[0].Type);
        }

        [Fact]
        public void IgnoreNameStartingWithNumber()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "0name")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("0name", input[0].Value);
            Assert.Equal(Stage1Types.Text, input[0].Type);
        }

        [Fact]
        public void IgnoreNameWithIllegalChar()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "name~")
            };
            new NameIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("name~", input[0].Value);
            Assert.Equal(Stage1Types.Text, input[0].Type);
        }
    }
}
